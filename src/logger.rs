use crate::string_builder::StringBuilder;

pub struct Logger;

impl Logger {
    pub fn log_info(message: &str){
        let message = StringBuilder::new()
            .push("[INFO]: ")
            .push(&*message)
            .build();
        println!("{}",message)
    }

    pub fn log_warn(message: &str){
        let message = StringBuilder::new()
            .push("[WARN]: ")
            .push(message)
            .build();
        println!("{}",message)
    }
}