mod event_handler;
mod logger;
mod string_builder;
mod command;
mod user_data;
mod helper_utils;

#[path="commands/poll_commands.rs"]
mod poll_commands;

#[path="commands/stat_commands.rs"]
mod stat_commands;

#[path="commands/moderation_commands.rs"]
mod mod_commands;

#[path = "commands/misc_commands.rs"]
mod misc_commands;

#[path = "misc/chat_relay.rs"]
mod chat_relay;

use serenity::{
    prelude::*
};
use crate::event_handler::Handler;
use serenity::client::bridge::gateway::GatewayIntents;
use std::fs::{File, OpenOptions};
use ron::de::from_reader;
use serde::{Deserialize};
use crate::logger::Logger;
use crate::string_builder::StringBuilder;
use crate::user_data::UserData;
use lazy_static::lazy_static;
use std::collections::HashMap;
use std::sync::{Arc,Mutex};
use std::io::Write;
use ron::to_string;
use timer::Timer;
use chrono::Duration;

#[derive(Deserialize)]
pub struct Config {
    bot_token: String,
    poll_channel_id: u64,
    help_channel_id: u64,
    welcome_channel_id: u64,
}

struct ShutdownHook;

impl ShutdownHook{
    fn save(&mut self) {
        let mut f = OpenOptions::new()
            .create(true)
            .write(true)
            .open("users.ron").expect("Failed opening file users.ron for saving.");
        let udata = USER_DATA.lock().unwrap();
        let content = match to_string(&*udata){
            Ok(n) => n,
            Err(e) => {
                println!("Unexpected error writing save data: {}",e);
                return;
            }
        };
        let buf_vec: Vec<u8> = content.chars().map(|x| x as u8).collect();
        let buf: &[u8] = &buf_vec;
        
        match f.write_all(buf){
            Ok(_) => {},
            Err(e) => {
                println!("Unexpected error during save: {}",e);
                return;
            }
        }

        match f.sync_all() {
            Ok(_) => {},
            Err(e) => {
                println!("Unexpected error during save: {}",e);
                return;
            }
        }
    }
}

lazy_static!(
    pub static ref CONFIG: Config = {
        let f = File::open("settings.ron").expect("Failed to open file");
        return from_reader(f).expect("Failed to load config")
    };

    pub static ref USER_DATA: Arc<Mutex<HashMap<u64,UserData>>> = {
        let f = match File::open("users.ron"){
            Ok(n) => n,
            Err(_) => {
                Logger::log_warn("No users.ron found, not loading...");
                return Arc::from(Mutex::from(HashMap::new()));
            }
        };

        let map: HashMap<u64,UserData> = from_reader(f).expect("Failed reading user data.");

        return Arc::from(Mutex::from(map))
    };

    pub static ref EXIT_LOCK: Mutex<i8> = Mutex::from(0);
);

#[tokio::main]
async fn main() {
    let mut _shutdown = ShutdownHook;

    let timer = Timer::new();
    let _= timer.schedule_repeating(Duration::minutes(5),move || {
        let _tmp = EXIT_LOCK.lock().unwrap();
        _shutdown.save()
    });

    Logger::log_info(&*StringBuilder::new()
        .push(&*format!("Using token: {}", &CONFIG.bot_token))
        .build());

    let mut client = Client::builder(&CONFIG.bot_token)
        .event_handler(Handler)
        .intents(GatewayIntents::privileged() | GatewayIntents::non_privileged())
        .await
        .expect("Error creating or initializing the client.");
        
    if let Err(why) = client.start().await {
        println!("Client error: {:?}", why);
    }
}
