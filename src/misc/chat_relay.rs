use serenity::{builder::CreateMessage, model::id::ChannelId};

use crate::{event_handler::MsgData, string_builder::StringBuilder};

use crate::CONFIG;


pub async fn relay(msg: String, data: MsgData){
    let relay_channel = ChannelId::from(874122998725099611);
    let message = StringBuilder::new()
        .push(&msg)
        .build();

        let _ = match relay_channel.send_message(&data.ctx.http, |m: &mut CreateMessage| {
        m.embed(|e| {
            e.title(&data.author.id.as_u64());
            e.author(|a| {
                if let Some(author_ava) = &data.author.avatar_url(){
                    a.icon_url(author_ava);
                }
                a.name(&data.author.name);
                a
            });
            e.description(&message)
        });
        m
    }).await{
        Ok(_) => {},
        Err(e) => {
            println!("Failed sending relay embed: {}", e);
            return;
        }
    };
}

pub async fn relay_reply(msg: String, data: MsgData){
    let channel = ChannelId::from(CONFIG.help_channel_id.to_owned());
    let message = StringBuilder::new()
    .push(&msg)
    .build();

    let _ = match channel.send_message(&data.ctx.http, |m: &mut CreateMessage| {
        m.embed(|e| {
            e.author(|a| {
                if let Some(author_ava) = &data.author.avatar_url(){
                    a.icon_url(author_ava);
                }
                a.name(&data.author.name);
                a
            });
            e.description(&message)
        });
        m
    }).await{
        Ok(_) => {},
        Err(e) => {
            println!("Failed sending relay embed: {}", e);
            return;
        }
    };
}