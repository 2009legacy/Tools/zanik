use serenity::model::channel::ReactionType;
use serenity::model::id::EmojiId;
use crate::event_handler::MsgData;
use serenity::model::Permissions;
use crate::string_builder::StringBuilder;
use serenity::model::misc::Mentionable;

pub fn parse_emoji_from_str(string: &str) -> ReactionType{
    let split: Vec<&str> = string.split(":").collect();
    let mut id = String::from(split[2]);
    let _ = id.pop();
    let name = Option::from(String::from(split[1]));

    return ReactionType::Custom {
        animated: false,
        id: EmojiId(id.parse::<u64>().expect("Failed parsing id as u64.")),
        name
    }
}

pub async fn message(data: &MsgData, msg: &str){
    data.channel_id.say(&data.ctx.http,msg).await.expect("Failed to send message");
}

pub async fn assert_admin(data: &MsgData) -> bool{
    if let Some(guild) = &data.ctx.cache.guild(&data.guild_id).await {
        let permissions: Permissions = match guild.member_permissions(&data.ctx.http, &data.author.id).await {
            Ok(n) => n,
            Err(e) => {
                println!("Failed getting user permissions: {}", e);
                return false;
            }
        };

        if !permissions.administrator() {
            let message = StringBuilder::new()
                .push(&data.author.mention().to_string())
                .push(", you must be an admin to use this command.")
                .build();
            &data.channel_id.say(&data.ctx.http, message).await;
            return false;
        }
    }
    true
}