use std::process::exit;

use crate::helper_utils::{message,assert_admin};
use crate::poll_commands::*;
use crate::stat_commands::*;
use crate::mod_commands::*;
use crate::misc_commands::*;
use crate::EXIT_LOCK;
use crate::event_handler::MsgData;


pub struct Command<'a> {
    pub(crate) name: &'a str,
    pub(crate) msg_data: MsgData,
    pub(crate) poll_channel: u64
}

impl Command<'_> {
    pub async fn handle(&self){
        match self.name {
            "woah"      => message(&self.msg_data,"scam!").await,
            "*poll"     => build_poll(&self.msg_data,self.poll_channel.to_owned()).await,
            "*endpoll"  => close_poll(&self.msg_data,self.poll_channel.to_owned()).await,
            "*level"    => print_level(&self.msg_data).await,
            "*top"      => print_top_10(&self.msg_data).await,
            "*exit"     => attempt_safe_exit(&self.msg_data).await,
            "*clear"    => clear_n(&self.msg_data).await,
            "*wiki"     => send_wiki(&self.msg_data).await,
            "*commands" => send_commands(&self.msg_data).await,
            "*help"     => send_commands(&self.msg_data).await,
            "*2.009"    => send_progress(&self.msg_data).await,
            "*tickets"  => show_ticket_help(&self.msg_data.ctx.http, &self.msg_data.channel_id).await,
            "*goblify"  => goblify(&self.msg_data).await,
            _ => {}
        }
    }
}

async fn attempt_safe_exit(data: &MsgData){
    if assert_admin(&data).await {
        {
            let tmp = EXIT_LOCK.lock().unwrap();
        }
        message(&data,"Safely exiting...").await;
        exit(0);
    }
}
