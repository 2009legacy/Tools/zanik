use std::u64;

use serenity::prelude::*;
use serenity::model::prelude::Message;
use serenity::model::gateway::{Ready, Activity};
use serenity::async_trait;
use serenity::model::id::{ChannelId, GuildId, MessageId};
use serenity::utils::MessageBuilder;
use serenity::model::user::User;
use serenity::model::guild::Member;
use serenity::model::event::TypingStartEvent;
use crate::chat_relay::{relay, relay_reply};
use crate::mod_commands::show_ticket_help;
use crate::string_builder::StringBuilder;
use crate::logger::Logger;
use crate::command::Command;
use crate::CONFIG;
use crate::USER_DATA;
use crate::user_data::UserData;
use serenity::model::channel::{MessageType, PrivateChannel};

pub struct Handler;
pub struct MsgData {
    pub(crate) channel_id: ChannelId,
    pub(crate) ctx: Context,
    pub(crate) args: Vec<String>,
    pub(crate) author: User,
    pub(crate) guild_id: GuildId,
    pub(crate) msg_id: MessageId
}

#[async_trait]
impl EventHandler for Handler {
    async fn guild_member_addition(&self, _ctx: Context, _guild_id: GuildId, _new_member: Member) {
        let general_channel = ChannelId(CONFIG.welcome_channel_id);
        let message = MessageBuilder::new()
            .push("Welcome to 2009Scape, ")
            .push(_new_member.user.mention().to_string())
            .push("! I've sent you a DM with useful info! We hope you enjoy your stay!")
            .build();
        general_channel.say(&_ctx.http,message).await.expect("Failed to send message.");

        let dm_channel: PrivateChannel = match _new_member.user.create_dm_channel(&_ctx.http).await{
            Ok(n) => n,
            Err(e) => {
                println!("Failed opening DM channel: {}",e);
                return;
            }
        };

        

        let dm_message = StringBuilder::new()
            .push("Hi, welcome to our amazing community!\n")
            .push("I just wanted to drop off some helpful information for you:\n")
            .push("[Getting Started](https://2009scape.org/site/game_guide/how_do_i_get_started0.html)\n")
            .push("[Controls](https://2009scape.org/site/game_guide/controls.html)\n")
            .push("[Our Credit System](https://2009scape.org/site/game_guide/credits.html)\n")
            .push("[More about me](https://2009scape.org/site/game_guide/zanik.html)\n")
            .push("[Our Staff](https://2009scape.org/site/community/staff.html)\n\n")
            .push("If you have any further questions please make sure to check the #faq channel!")
            .build();


        let _ = match dm_channel.id.send_message(&_ctx.http, |m| {
            m.embed(|e|{
                e.title("Welcome to 2009scape!");
                e.description(&dm_message)
            });

            m
        }).await{
            Ok(n) => n,
            Err(e) => {
                println!("Failed sending welcome embed: {}", e);
                return;
            }
        };
    }

    // async fn guild_member_removal(&self, _ctx: Context, _: GuildId, _user: User, _member_data_if_available: Option<Member>, ) {
    //     let general_channel = ChannelId(CONFIG.welcome_channel_id);
    //     let message = MessageBuilder::new()
    //         .push("So long, ")
    //         .push(&_user.name.to_string())
    //         .push(".")
    //         .build();

    //     general_channel.say(_ctx.http,message).await.expect("Error sending message.");
    // }


    async fn message(&self, ctx: Context, msg: Message){
        //799921459640008734
        if msg.author.bot{
            return;
        }

        {
            let mut datamap = USER_DATA.lock().unwrap();
            {
                let mut new_data = UserData::new(*msg.author.id.as_u64());

                let udata = match datamap.get_mut(msg.author.id.as_u64()) {
                    Some(n) => n,
                    None => &mut new_data
                };

                udata.add_experience(2);
            }
            let has_key = datamap.contains_key(msg.author.id.as_u64());

            if !has_key {
                datamap.insert(*msg.author.id.as_u64(), UserData{
                    level: 1,
                    experience: 1,
                    next_level: 100,
                    user_id: *msg.author.id.as_u64()
                });
            }
        }

        let mut split = msg.content.split(" ");
        let command = split.next().unwrap_or("");
        let mut args: Vec<String> = split.map(|x| String::from(x)).collect();
        args.retain(|x| x != command);

        let msg_data = MsgData{
            channel_id: msg.channel_id,
            ctx,
            args,
            author: msg.author,
            guild_id: msg.guild_id.unwrap(),
            msg_id: msg.id
        };

        let ticket_channel_id: u64 = 874122998725099611;

        if msg.channel_id.as_u64().to_owned() == ticket_channel_id {
            if msg.kind == MessageType::InlineReply {
                relay_reply(msg.content, msg_data).await;
                return;
            }
       }

        if msg.channel_id.as_u64() == &CONFIG.help_channel_id { //Live server help channel
            relay(msg.content, msg_data).await;
            return;
        }

        let command = Command {
            name: command,
            msg_data,
            poll_channel: CONFIG.poll_channel_id.to_owned()
        };


        command.handle().await;
    }

    async fn ready(&self,ctx: Context, _: Ready){
        ctx.set_activity(Activity::playing("2009scape")).await;
        Logger::log_info("Connected.")
    }

    async fn reaction_add(&self, _ctx: Context, _add_reaction: serenity::model::channel::Reaction) {
        if _add_reaction.channel_id.as_u64().to_owned() != 874122998725099611 as u64 {
            return;
        }
        let msg = match _ctx.http.get_message(_add_reaction.channel_id.as_u64().to_owned(), _add_reaction.message_id.as_u64().to_owned()).await {
            Ok(n) => {n},
            Err(e) => {
                println!("Error getting message: {}", e);
                return
            }
        };

        let _reaction = _add_reaction.emoji.to_string();

        if let Some(author_id) = &msg.embeds[0].title {
            let _author_id = match author_id.parse::<u64>() {
                Ok(n) => n,
                Err(e) => {
                    println!("Failed converting author ID to u64: {}", e);
                    return
                }
            };

            let _user = match _ctx.http.get_user(_author_id).await{
                Ok(n) => n,
                Err(e) => {
                    println!("Failed getting user: {}", e);
                    return
                }
            };

            let _message = match &*_reaction {
                "👍" => {
                    MessageBuilder::new()
                        .mention(&_user)
                        .push(" Done!")
                        .build()
                }

                "👎" => {
                    MessageBuilder::new()
                        .mention(&_user)
                        .push(" I can not do that right now.")
                        .build()
                }

                "📰" => {
                    MessageBuilder::new()
                        .mention(&_user)
                        .push(" Please create an issue for this on our gitlab. https://gitlab.com/2009scape/2009scape/-/issues/new?issue%5Bmilestone_id%5D=")
                        .build()
                }

                "❓" => {
                    MessageBuilder::new()
                        .mention(&_user)
                        .push(" Please read the FAQ - it was created to serve you, the player :)")
                        .build()
                }

                "❗" => {
                    MessageBuilder::new()
                        .mention(&_user)
                        .push(" Please re-read the rules - they are there to serve you and the community :)")
                        .build()
                }

                "✅" => {
                    MessageBuilder::new()
                    .mention(&_user)
                    .push(" You're welcome! :)")
                    .build()
                }

                _ => {
                    show_ticket_help(&_ctx.http, &_add_reaction.channel_id).await;
                    println!("User tried: {}", &_reaction);
                    return
                }
            };

            
            let channel = ChannelId::from(CONFIG.help_channel_id.to_owned());

            match channel.say(&_ctx.http, _message).await{
                Ok(_) => {},
                Err(e) => {
                    println!("Error sending message: {}", e);
                    return
                }
            };
        }
    }

    async fn typing_start(&self, _ctx: Context, _tse: TypingStartEvent) {
        /*let message = MessageBuilder::new()
            .mention(&_tse.user_id)
            .push(", quit typing!")
            .build();
        _tse.channel_id.say(_ctx.http,message).await.expect("Failed to send message");*/
    }


}
