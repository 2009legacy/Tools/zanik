use crate::event_handler::MsgData;
use crate::user_data::UserData;
use crate::string_builder::StringBuilder;
use serenity::model::id::UserId;
use crate::USER_DATA;
use crate::helper_utils::{message};


pub async fn print_top_10(data: &MsgData){
    let guild_id = &data.guild_id;

    let mut sorted_udata: Vec<UserData> = Vec::new();

    let mut builder = StringBuilder::new();
    builder.push(&*format!("```\nTop 10 for {}:\n", guild_id.name(&data.ctx).await.expect("Error fetching guild name.")));

    {
        let udata = USER_DATA.lock().unwrap();
        for data in udata.values(){
            sorted_udata.push(UserData::duplicate(data));
        }
        sorted_udata.sort_unstable_by(|a,b| {
            let mut sumb: u32 = 0;
            for i in 0..=b.level{
                sumb += i as u32
            }

            let mut suma: u32 = 0;
            for i in 0..=a.level{
                suma += i as u32
            }

            let expa: u32 = 100 + (suma * 50) + (a.experience as u32);
            let expb: u32 = 100 + (sumb * 50) + (b.experience as u32);

            expb.cmp(&expa)
        });
    }
    let max_count;

    if sorted_udata.len() > 10 {
        max_count = 10;
    }  else {
        max_count = sorted_udata.len()
    };

    let mut counter = 0;
    let mut i = 0;

    while counter < max_count {
        let udata: &UserData = &sorted_udata[i];
        let level = udata.level;
        let user = UserId(udata.user_id);
        let mem_get = &data.ctx.http.get_member(*guild_id.as_u64(),*user.as_u64()).await;
        let member = match mem_get {
            Ok(n) => n,
            Err(_) => {
                println!("Skipping {}, user no longer exists.",user.as_u64());
                i += 1;
                continue
            }
        };

        builder.push(&*format!("{}. {} - Level {}\n", counter + 1, member.user.name, level));
        i += 1;
        counter += 1;
    }

    builder.push("```");
    message(&data,&*builder.build()).await;
    drop(sorted_udata);
}

pub async fn print_level(data: &MsgData){
    let mut msg = String::new();

    {
        let udata = USER_DATA.lock().unwrap();
        let user = match udata.get(data.author.id.as_u64()) {
            Some(n) => n,
            None => return
        };
        msg.push_str(&*user.as_string())
    }

    message(data, &*msg).await
}
