use crate::event_handler::MsgData;
use serenity::model::channel::PrivateChannel;
use crate::string_builder::StringBuilder;
use rand::{Rng, thread_rng};

pub async fn send_wiki(data: &MsgData){
    let wiki_term = data.args.join("_");
    let link = format!("https://runescape.wiki/w/{}?action=history&year=2009&month=1&tagfilter=",wiki_term);

    &data.channel_id.say(&data.ctx.http,link).await;
}

pub async fn send_commands(data: &MsgData){
    let dm_channel: PrivateChannel = match data.author.create_dm_channel(&data.ctx.http).await {
        Ok(n) => n,
        Err(e) => {
            println!("Failed creating private channel: {}",e);
            return;
        }
    };

    let commands = StringBuilder::new()
        .push("====NORMAL COMMANDS====\n")
        .push("`*wiki thing` - returns a link to the history page for `thing` pointed to 2009.\n")
        .push("`*level` - shows your level and progress towards your next.\n")
        .push("`*top` - shows the top 10 members of the current discord sorted by xp.\n")
        .push("\n\n====ADMIN-ONLY====\n")
        .push("`*poll subject text here :: emoji1 option1, emoji2 option2` - Runs a poll with the given subject text and reactions/emojis\n")
        .push("`*endpoll message_id` - edits the given message with end-of-poll text.\n")
        .push("`*clear n` - clears the last n messages in the current channel\n")
        .push("`*exit` - Safely exits the bot\n")
        .build();

    match dm_channel.say(&data.ctx.http,commands).await {
        Ok(_) => {},
        Err(e) => {
            println!("Error sending message: {}",e);
        }
    }
}

pub async fn send_progress(data: &MsgData){
    let progress_msg = StringBuilder::new()
        .push("2.009 Roadmap\nKey: `INP` - In Progress, `NTS` - Not Started, `FIN` - Finished/ready\n")
        .push("Completed Items: 19/44 (43%)\n\n")
        .push("[FIN] Core Networking\n")
        .push("[FIN] Login\n")
        .push("[FIN] Registration\n")
        .push("[FIN] Rendering\n")
        .push("[FIN] Interfaces\n")
        .push("[FIN] Movement\n")
        .push("[FIN] NPCs\n")
        .push("[FIN] Items\n")
        .push("[FIN] Objects\n")
        .push("[FIN] Content API\n")
        .push("[FIN] Door Handling\n")
        .push("[FIN] Dialogue System\n")
        .push("[FIN] Core Skill System\n")
        .push("[FIN] Melee Combat\n")
        .push("[NTS] Ranged Combat\n")
        .push("[NTS] Mage Combat\n")
        .push("[FIN] Woodcutting\n")
        .push("[NTS] Fishing\n")
        .push("[NTS] Mining\n")
        .push("[NTS] Firemaking\n")
        .push("[FIN] Cooking\n")
        .push("[NTS] Smithing\n")
        .push("[NTS] Crafting\n")
        .push("[NTS] Utility Magic\n")
        .push("[NTS] Prayer\n")
        .push("[FIN] Quest System\n")
        .push("[FIN] Cook's Assistant\n")
        .push("[FIN] Rune Mysteries\n")
        .push("[NTS] Doric's Quest\n")
        .push("[INP] Sheep Shearer\n")
        .push("[NTS] Goblin Diplomacy\n")
        .push("[NTS] Imp Catcher\n")
        .push("[NTS] The Knight's Sword\n")
        .push("[NTS] The Restless Ghost\n")
        .push("[NTS] Vampire Slayer\n")
        .push("[NTS] Witch's Potion\n")
        .push("[NTS] Romeo & Juliet\n")
        .push("[NTS] Pirate's Treasure\n")
        .push("[NTS] Shield of Arrav\n")
        .push("[NTS] Demon Slayer\n")
        .push("[NTS] Ernest the Chicken\n")
        .push("[NTS] Prince Ali Rescue\n")
        .push("[NTS] Black Knights' Fortress\n")
        .push("[NTS] Dragon Slayer\n")
        .push("[NTS] Tutorial Island\n")
        .push("------Beta Launch-----\n\n")
        .push("Things on this list may be added/removed/moved around at any time.")
        .build();

    let _dm_channel: PrivateChannel = match data.author.create_dm_channel(&data.ctx.http).await {
        Ok(n) => n,
        Err(e) => {
            println!("Failed creating DM channel: {}",e);
            return;
        }
    };

    match _dm_channel.say(&data.ctx.http,progress_msg).await {
        Ok(_) => {},
        Err(e) => {
            println!("Error sending message: {}",e);
        }
    }
}

pub async fn goblify(data: &MsgData){
    let mut prefix_index = 0;
    let mut suffix_index = 0;
    {
        let mut rng = thread_rng();
        prefix_index = rng.gen_range(0..31);
        suffix_index = rng.gen_range(0..31);
    }

    let prefix = match prefix_index {
        0 => "Beetle",
        1 => "Bent",
        2 => "Blob",
        3 => "Bone",
        4 => "Dirt",
        5 => "Earth",
        6 => "Fat",
        7 => "Foul",
        8 => "Frog",
        9 => "Grass",
        10 => "Grub",
        11 => "Lump",
        12 => "Maggot",
        13 => "Moss",
        14 => "Mud",
        15 => "Roach",
        16 => "Slime",
        17 => "Slug",
        18 => "Small",
        19 => "Smelly",
        20 => "Snail",
        21 => "Snow",
        22 => "Snot",
        23 => "Stupid",
        24 => "Thick",
        25 => "Thin",
        26 => "Toad",
        27 => "Ugly",
        28 => "Wart",
        29 => "Wood",
        30 => "Worm",
        _ => ""
    };

    let suffix = match suffix_index {
        0 => "arms",
        1 => "beard",
        2 => "blood",
        3 => "bones",
        4 => "bottom",
        5 => "brain",
        6 => "brains",
        7 => "chin",
        8 => "ears",
        9 => "eye",
        10 => "eyes",
        11 => "face",
        12 => "feet",
        13 => "finger",
        14 => "fingers",
        15 => "fists",
        16 => "foot",
        17 => "hair",
        18 => "hands",
        19 => "head",
        20 => "knees",
        21 => "knuckles",
        22 => "legs",
        23 => "nails",
        24 => "neck",
        25 => "nose",
        26 => "teeth",
        27 => "thighs",
        28 => "thumb",
        29 => "toes",
        30 => "tongue",
        _ => ""
    };

    let name = StringBuilder::new().push(prefix).push(suffix).build();
    data.guild_id.edit_member(&data.ctx.http, &data.author.id, |m| {
        m.nickname(name)
    }).await.expect("Unable to set nickname.");
}
