use std::sync::Arc;

use serenity::http::{CacheHttp, Http};
use serenity::model::id::ChannelId;
use serenity::utils::MessageBuilder;

use crate::event_handler::MsgData;
use crate::helper_utils::{assert_admin, parse_emoji_from_str};
use crate::string_builder::StringBuilder;

pub async fn clear_n(data: &MsgData){
    if assert_admin(&data).await {
        let _msg_count_str: String = match *&data.args.get(0) {
            Some(n) => String::from(n),
            None => String::from("5")
        };

        let msg_count = _msg_count_str.parse::<u64>().unwrap_or(5 as u64);

        if msg_count < 2 || msg_count > 100 {
            let _msg = MessageBuilder::new()
                .mention(&data.author)
                .push(" I'm sorry, but for my bulk deletion services I require a minimum of 2 or a maximum of 100 messages to delete.")
                .build();
            &data.channel_id.say(&data.ctx.http(), _msg);
            return;
        }

        let _messages = match data.channel_id.messages(&data.ctx.http, |retriever| {
            retriever.before(&data.msg_id).limit(msg_count)
        }).await {
            Ok(n) => n,
            Err(e) => {
                println!("Failed retrieving prior {} messages: {}",msg_count,e);
                return
            }
        };

        let _ = match &data.channel_id.delete_messages(&data.ctx.http(), _messages).await {
            Ok(_) => {},
            Err(e) => {
                println!("Failed deleting prior {} messages: {}", msg_count, e);
                return
            }
        };

        &data.channel_id.say(&data.ctx.http,format!("Purged previous {} messages.",msg_count)).await;
    }
}

pub async fn show_ticket_help(http: &Arc<Http>, channel_id: &ChannelId){

    let _description = StringBuilder::new()
        .push("Reactions and Their Meanings\n")
        .push(":thumbsup:")
        .push(" - Informs the user that the request is complete \n\n")
        .push(":thumbsdown:")
        .push(" - Informs the user that the request cannot be completed \n\n")
        .push(":newspaper:")
        .push(" - Requests that the user create a Gitlab issue \n\n")
        .push(":question:")
        .push(" - Requests that the user checks the FAQ\n\n")
        .push(":exclamation:")
        .push(" - Requests the user to re-read the rules \n")
        .build();

    match channel_id.send_message(&http, |m| {
        m.embed(|e| {
            e.title("Ticket System Help");
            e.description(_description);
            e
        });
        m
    }).await {
        Ok(_) => {},
        Err(e) => println!("Error sending ticket help: {}", e)
    }
}