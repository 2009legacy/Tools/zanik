use serenity::model::id::ChannelId;
use serenity::utils::MessageBuilder;

use crate::helper_utils::{parse_emoji_from_str,assert_admin};
use crate::string_builder::StringBuilder;
use crate::event_handler::MsgData;



pub async fn build_poll(data: &MsgData,poll_channel_id: u64){
    if assert_admin(&data).await {
        let poll_channel = ChannelId(poll_channel_id);

        let _option_temp_rejoin: String = data.args.join(" ");
        let _option_args_split: Vec<&str> = _option_temp_rejoin.split("::").collect();

        let has_custom_options = _option_args_split.len() > 1;

        if has_custom_options {
            let _options_pre_split = _option_args_split[1];
            let _options_split: Vec<&str> = _options_pre_split.split(",").collect();
            let _option_a = _options_split[0].trim();
            let _option_b = _options_split[1].trim();

            let mut _option_a_split: Vec<&str> = _option_a.split(" ").collect();
            let mut _option_b_split: Vec<&str> = _option_b.split(" ").collect();

            let emoji_a = parse_emoji_from_str(_option_a_split[0].trim());
            _option_a_split.retain(|x| x.trim() != emoji_a.to_string());
            let text_a = _option_a_split.join(" ");

            let emoji_b = parse_emoji_from_str(_option_b_split[0].trim());
            _option_b_split.retain(|x| x.trim() != emoji_b.to_string());
            let text_b = _option_b_split.join(" ");

            let poll_message = MessageBuilder::new()
                .push(_option_args_split[0])
                .push("\n\n")
                .push(emoji_a.to_string())
                .push(text_a)
                .push("\n")
                .push(emoji_b.to_string())
                .push(text_b)
                .build();

            let _msg = poll_channel.say(&data.ctx.http,poll_message).await.expect("Error sending message");
            _msg.react(&data.ctx.http,emoji_a).await.expect("Error adding reaction.");
            _msg.react(&data.ctx.http,emoji_b).await.expect("Error adding reaction.");
        } else {
            let p_crown_reaction = parse_emoji_from_str("<:Purple_partyhat:700168465989894155>");
            let y_crown_reaction = parse_emoji_from_str("<:Yellow_partyhat:700168466027642941>");

            let poll_message = MessageBuilder::new()
                .push(data.args.join(" "))
                .push("\n\n")
                .push(p_crown_reaction.to_string())
                .push(" Yes \n")
                .push(y_crown_reaction.to_string())
                .push(" No")
                .build();
            let _msg = poll_channel.say(&data.ctx.http, poll_message).await.expect("Unable to send message to poll channel.");


            _msg.react(&data.ctx.http, p_crown_reaction).await.expect("Error adding reaction.");
            _msg.react(&data.ctx.http, y_crown_reaction).await.expect("Error adding reaction.");
        }
    }
}

pub async fn close_poll(data: &MsgData,poll_channel: u64){
    if assert_admin(&data).await {
        let _msg_id_raw = &data.args[0].parse::<u64>().expect("Error parsing message ID as u64");

        let mut close_msg = String::new();

        if data.args.len() > 1 {
            let mut _newargs = data.args.to_owned();
            _newargs.retain(|x| x as &str != &*_msg_id_raw.to_string());
            close_msg = _newargs.join(" ");
        }

        let mut message = data.ctx.http.get_message(poll_channel, _msg_id_raw.to_owned()).
            await.
            expect("Unable to find message.");

        let new_content = StringBuilder::new()
            .push(&message.content)
            .push("\n **THIS POLL HAS BEEN CLOSED**")
            .push(" ")
            .push(&*close_msg)
            .build();

        message.edit(&data.ctx,|m| m.content(new_content)).await.expect("Unable to edit the poll message");
    }
}
