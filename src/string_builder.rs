pub struct StringBuilder{
    content: String
}

impl StringBuilder{
    pub fn new() -> StringBuilder{
        StringBuilder{
            content: String::new()
        }
    }

    pub fn push(&mut self, string: &str) -> &mut StringBuilder {
        self.content.push_str(string);
        self
    }

    pub fn build(&self) -> String{
        String::from(&self.content)
    }
}
