use serde::{Deserialize,Serialize};
use crate::string_builder::StringBuilder;

#[derive(Serialize,Deserialize)]
pub struct UserData {
    pub(crate) level: u8,
    pub(crate) experience: u16,
    pub(crate) next_level: u16,
    pub(crate) user_id: u64
}

impl UserData {
    pub fn add_experience(&mut self,amount: u16){
        if &self.experience + amount > *&self.next_level{
            self.level += 1;
            self.experience += amount;
            self.experience = self.experience - self.next_level;
            self.next_level = (100 + (((self.level as u16) - 1) * 50)) as u16;
        } else {
            self.experience += amount;
        }
    }

    pub fn as_string(&self) -> String {
        let mut builder = StringBuilder::new();
        let _ = builder.push("```\n");
        let _ = builder.push(&*format!("Level: {}\n", &self.level))
            .push("[");
        let percentages = vec![0.19,0.39,0.59,0.79,0.99];

        let level_percentage: f64 = *&self.experience as f64 / *&self.next_level as f64;

        for perc in percentages{
            if level_percentage >= perc {
                builder.push("#");
            } else {
                builder.push("_");
            }
        };

        builder.push(&*format!("] ({}/{})",&self.experience,&self.next_level));
        builder.push("```");
        builder.build()
    }

    pub fn new(user_id: u64) -> UserData{
        UserData {
            level: 1,
            experience: 0,
            next_level: 100,
            user_id
        }
    }

    pub fn duplicate(other: &UserData) -> UserData{
        UserData{
            level: *&other.level,
            experience: *&other.experience,
            next_level: *&other.next_level,
            user_id: *&other.user_id
        }
    }
}
